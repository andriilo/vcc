const http = require("http");

const hostname = '0.0.0.0';
const port = 8888;

http.createServer((req, res) => {
    if ((id = req.url.match("^/([a-z]+)$"))) {
        if (req.method === "GET") {
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Hello '+id[1]);
        }
    } else {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('hello world');
    }
}).listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
